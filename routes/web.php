<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'contactFormView', 'uses' => 'ContactForm@form']);

Route::post('/save', ['as' => 'contactFormSave', 'uses' => 'ContactForm@save']);

Route::get('/getleads', ['as' => 'contactFormGetCSV', 'uses' => 'ContactForm@getCSV']);
