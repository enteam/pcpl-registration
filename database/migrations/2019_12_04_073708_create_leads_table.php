<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->string('first_name', 255);
          $table->string('last_name', 255);
          $table->string('mobile_no', 255);
          $table->string('email', 255);
          $table->string('is_agent')->nullable();
          $table->string('maharera_no', 255)->nullable();
          $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leads');
    }
}
