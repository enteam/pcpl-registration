<!DOCTYPE html>
<html lang="en">
<head>
	<title>PCPL</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="{{ asset('public/images/PCPL_Logo.png') }}"/>
<!--===============================================================================================-->
	<link href="{{ asset('public/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" >
<!--===============================================================================================-->
	<link href="{{ asset('public/fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" >
<!--===============================================================================================-->
	<link href="{{ asset('public/vendor/animate/animate.css') }}" rel="stylesheet" type="text/css" >
<!--===============================================================================================-->
	<!-- <link href="{{ asset('public/vendor/css-hamburgers/hamburgers.min.css') }}" rel="stylesheet" type="text/css" > -->
<!--===============================================================================================-->
	<link href="{{ asset('public/vendor/animsition/css/animsition.min.css') }}" rel="stylesheet" type="text/css" >
<!--===============================================================================================-->
	<link href="{{ asset('public/vendor/select2/select2.min.css') }}" rel="stylesheet" type="text/css" >
<!--===============================================================================================-->
	<!-- <link href="{{ asset('public/vendor/daterangepicker/daterangepicker.css') }}" rel="stylesheet" type="text/css" > -->
<!--===============================================================================================-->
	<link href="{{ asset('public/css/util.css?v1') }}" rel="stylesheet" type="text/css" >
	<link href="{{ asset('public/css/main.css?v1') }}" rel="stylesheet" type="text/css" >
<!--===============================================================================================-->
</head>
<body>


	<div class="container-contact100">
		<div class="wrap-contact100">
			<span class="contact100-form-title">
				<img src="{{ asset('public/images/PCPL_Logo.png') }}" alt="PCPL Logo">
			</span>
			<br>
			<div class="response-message" style="display:none;">
				<h1 style="text-align: center; color: #005596;">Thank You.</h1>

				<br>
				<br>

				<div class="container-contact100-form-btn go-back">
					<div class="wrap-contact100-form-btn">
						<div class="contact100-form-bgbtn"></div>
						<button class="contact100-form-btn">
							<span>
								Go Back
								<i class="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i>
							</span>
						</button>
					</div>
				</div>
				<br>
				<br>

					<div class="container-contact100-form-btn go-back" onclick="window.location='https://PranavConstructions.com';">
						<div class="wrap-contact100-form-btn">
							<div class="contact100-form-bgbtn"></div>
							<button class="contact100-form-btn"  style="background-color: #005596 !important">
								<span>
									Visit PranavConstructions.com
									<i class="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i>
								</span>
							</button>
						</div>
					</div>
			</div>

			<form class="contact100-form validate-form">


				<div class="wrap-input100 validate-input" data-validate="First Name is required">
					<span class="label-input100">First Name</span>
					<input class="input100" type="text" name="first_name" placeholder="Enter your first name">
					<span class="focus-input100"></span>
				</div>

				<div class="wrap-input100 validate-input" data-validate="Last Name is required">
					<span class="label-input100">Last Name</span>
					<input class="input100" type="text" name="last_name" placeholder="Enter your last name">
					<span class="focus-input100"></span>
				</div>

				<div class="wrap-input100 validate-input" data-validate = "Please enter valid mobile no">
					<span class="label-input100">Mobile No</span>
					<input class="input100" type="number" name="mobile_no" placeholder="Enter your mobile no">
					<span class="focus-input100"></span>
				</div>

				<div class="wrap-input100 validate-input" data-validate = "Please enter valid Email ID">
					<span class="label-input100">Email</span>
					<input class="input100" type="text" name="email" placeholder="Enter your email address">
					<span class="focus-input100"></span>
				</div>

				<div class="wrap-input100 input100-select">
					<span class="label-input100">Are you an Agent?</span>
					<br>

					&nbsp;
						<input class="" type="radio" name="is_agent" id="is_agent_yes" value="Yes">
						<label class="" for="is_agent_yes">
							Yes
						</label>
					&nbsp;
						<input class="" type="radio" name="is_agent" id="is_agent_no" value="No" checked>
						<label class="" for="is_agent_no">
							No
						</label>

					<span class="focus-input100"></span>
				</div>

				<div class="wrap-input100 validate-input">
					<span class="label-input100">MAHARERA Number</span>
					<input class="input100" type="text" name="maharera_no" placeholder="Enter your MAHARERA Number">
					<span class="focus-input100"></span>
				</div>

				<div class="container-contact100-form-btn">
					<div class="wrap-contact100-form-btn">
						<div class="contact100-form-bgbtn"></div>
						<button class="contact100-form-btn">
							<span>
								Submit
								<i class="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i>
							</span>
						</button>
					</div>
				</div>

			</form>
		</div>
	</div>

	<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
	<script type="text/javascript" src="{{ asset('public/vendor/jquery/jquery-3.2.1.min.js') }}"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="{{ asset('public/vendor/animsition/js/animsition.min.js') }}"></script>
<!--===============================================================================================-->
	<!-- <script type="text/javascript" src="{{ asset('public/vendor/bootstrap/js/popper.js') }}"></script> -->
	<script type="text/javascript" src="{{ asset('public/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<!--===============================================================================================-->
	<!-- <script type="text/javascript" src="{{ asset('public/vendor/select2/select2.min.js') }}"></script> -->
	<!-- <script>
		$(".selection-2").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect1')
		});
	</script> -->
<!--===============================================================================================-->
	<!-- <script type="text/javascript" src="{{ asset('public/vendor/daterangepicker/moment.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/vendor/daterangepicker/daterangepicker.js') }}"></script> -->
<!--===============================================================================================-->
	<script type="text/javascript" src="{{ asset('public/vendor/countdowntime/countdowntime.js') }}"></script>
<!--===============================================================================================-->
	<!-- <script type="text/javascript" src="{{ asset('public/js/main.js') }}"></script> -->

<script type="text/javascript">

(function ($) {
	// "use strict";


	/*==================================================================
	[ Focus Contact2 ]*/
	$('.input100').each(function(){
			$(this).on('blur', function(){
					if($(this).val().trim() != "") {
							$(this).addClass('has-val');
					}
					else {
							$(this).removeClass('has-val');
					}
			})
	});


	/*==================================================================
	[ Validate ]*/
	var first_name = $('.validate-input input[name="first_name"]');
	var last_name = $('.validate-input input[name="last_name"]');
	var email = $('.validate-input input[name="email"]');
	var mobile_no = $('.validate-input input[name="mobile_no"]');

	var maharera_no = $('.validate-input input[name="maharera_no"]');

	$('.validate-form').on('submit',function(){
			var check = true;

			if($(first_name).val().trim() == ''){
					showValidate(first_name);
					check=false;
			}

			if($(last_name).val().trim() == ''){
					showValidate(last_name);
					check=false;
			}

			if($(email).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
					showValidate(email);
					check=false;
			}

			if($(mobile_no).val().trim().length != 10){
				showValidate(mobile_no, "Please Enter Valid Mobile Number");
				check=false;
			}

			if($(maharera_no).val().trim().length != 0 && $(maharera_no).val().trim().length != 12){
					showValidate(maharera_no, "Please enter valid MAHARERA Number");
					check=false;
			}


			if(check) {

				$.ajax({
					url: "{{ route('contactFormSave') }}",
					type:'POST',
					data: {_token: "{{ csrf_token() }}",
									first_name:$(first_name).val().trim(),
									last_name:$(last_name).val().trim(),
									mobile_no:$(mobile_no).val().trim(),
									email:$(email).val().trim(),
									maharera_no:$(maharera_no).val().trim(),
									is_agent:$("input[name='is_agent']:checked"). val()
								},
					success: function(data) {
						console.table(data);
						if(data.success == true) {
							showSuccessMessage(data.success);
						} else {
							showVaildateFor(data.errors);
						}
					},
					error: function(data) {
						console.log(data.responseJSON.errors);
						$.each(data.responseJSON.errors, function (key, val) {
							showValidate($('.validate-input input[name="'+key+'"]'), val['0']);
							// alert(key + val);
						});
					}
				});

			}

			return false;

	});


	$('.validate-form .input100').each(function(){
			$(this).focus(function(){
				 hideValidate(this);
		 });
	});

	function showValidate(input, message) {

			var thisAlert = $(input).parent();

			if(message != '' && message != null) {
				thisAlert.attr('data-validate', message);
			}


			$(thisAlert).addClass('alert-validate');
	}

	function hideValidate(input) {
			var thisAlert = $(input).parent();

			$(thisAlert).removeClass('alert-validate');
	}

	function showSuccessMessage(message) {
		$('.validate-form').hide();
		$('.response-message').show();
	}


	function hideSuccessMessage() {
		$('.validate-form').show();
		$('.response-message').hide();
	}

	$('.go-back').on('click',function(){
		$('.validate-form').find('input').val('');
		$("#is_agent_yes").prop("checked", true);
		hideSuccessMessage();
	});

	function showVaildateFor(error_array) {
		var temp = error_array;
	}

})(jQuery);
</script>

</body>
</html>
