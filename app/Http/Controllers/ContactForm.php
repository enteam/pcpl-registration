<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use Validator;
use App\Leads;
use Illuminate\Support\Facades\Input;
use Laracsv\Export;

class ContactForm extends Controller
{
  /**
   * Display a listing of the myform.
   *
   * @return \Illuminate\Http\Response
   */
  public function form()
  {
    return view('contact_form');
  }

  /**
  * Display a listing of the myformPost.
  *
  * @return \Illuminate\Http\Response
  */
  public function save(Request $request)
  {


    $validator = Validator::make($request->all(), [
      'first_name' => 'required',
      'last_name' => 'required',
      'mobile_no' => 'required',
      'email' => 'required|email',
      'maharera_no' => 'between:12,12|nullable',
    ]);

    if ($validator->passes()) {

      // $lead_exists = Leads::select('mobile_no')
      //                      ->where('mobile_no', '=', Input::get('mobile_no'))
      //                      ->first();
      // dd($lead_exists);

      if(Leads::where('mobile_no', '=', Input::get('mobile_no'))->exists()) {
        return Response::json(array(
          'success' => false,
          'errors' => ['mobile_no' => ['Mobile no already exists']]
        ), 400); // 400 being the HTTP code for an invalid request.
      } else {
        $lead = new Leads();
        $lead->first_name  = Input::get('first_name');
        $lead->last_name   = Input::get('last_name');
        $lead->mobile_no   = Input::get('mobile_no');
        $lead->email       = Input::get('email');
        $lead->is_agent    = (Input::get('is_agent') == null || empty(Input::get('is_agent'))) ? "No" : Input::get('is_agent');
        $lead->maharera_no = Input::get('maharera_no');
        $lead->save();
        return Response::json(array('success' => true, 'message' => 'Thanks for your interest.'), 200);
      }
    }

    return Response::json(array(
      'success' => false,
      'errors' => $validator->getMessageBag()->toArray()
    ), 400); // 400 being the HTTP code for an invalid request.
  }

  public function getCSV()
  {

    $csvExporter = new \Laracsv\Export();
    $leads = Leads::get();

    // Register the hook before building
    $csvExporter->beforeEach(function ($leads) {
      $leads->created_at = date('d-m-Y H:i:s', strtotime($leads->created_at));
    });

    $csvExporter->build($leads, [
      'first_name' => 'First Name',
      'last_name' => 'Last Name',
      'mobile_no' => 'Mobile No',
      'email' => 'Email ID',
      'is_agent' => 'Agent',
      'maharera_no' => 'Maharera No',
      'created_at' => 'Created At'
    ])->download("PCPL Leads ".date('d-m-Y H-i-s').".csv");

  }


}
