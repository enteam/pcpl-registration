<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leads extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'mobile_no', 'email', 'is_agent', 'maharera_no'
    ];

    public $timestamps = false;
}
